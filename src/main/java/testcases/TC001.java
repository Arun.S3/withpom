package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import patternObjectMethod.LoginPage;
import wdMethods.TC00PackageMethod;

public class TC001 extends TC00PackageMethod{
	
	@BeforeTest
	public void setData() {
		//testCaseName ="Login Logout";
		datasheetname="loginCred";
		
	}
	@Test(dataProvider="fetchData")
	public void login(String Uname, String Password) {
		new LoginPage()
		.enterusername(Uname)
		.enterpassword(Password)
		.ClickSubmit();
	}
	

}
