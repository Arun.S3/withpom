package patternObjectMethod;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.TC00PackageMethod;

public class Homepage extends TC00PackageMethod  {
	
	
	public Homepage() {
		PageFactory.initElements(driver, this);
	}

	// page Factory @FindBy -->similar to your locate Elements
	
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") WebElement eleCRM;
	
	
	public MyHomePage ClickSubmit() {
		click(eleCRM);
		return new MyHomePage();

}
}
