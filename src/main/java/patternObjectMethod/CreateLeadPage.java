package patternObjectMethod;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.TC00PackageMethod;

public class CreateLeadPage extends TC00PackageMethod {
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	// page Factory @FindBy -->similar to your locate Elements
	
	@FindBy(how=How.CLASS_NAME, using="inputBox") WebElement eleCompNm;
	@FindBy(how=How.ID, using="createLeadForm_firstName")WebElement eleFnm;
	@FindBy(how=How.ID, using="createLeadForm_lastName")WebElement eleLNm;
	@FindBy(how=How.ID, using="createLeadForm_personalTitle")WebElement eleSal;
	@FindBy(how=How.ID, using="createLeadForm_primaryPhoneNumber")WebElement elePh;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit")WebElement eleClButton;
	
	//Enter Username
	
	public CreateLeadPage enterCnm (String Cnm) {
		type(eleCompNm,Cnm);
		return this;
	}
	
	public CreateLeadPage enterFnm (String Fnm) {
		type(eleFnm,Fnm);
		return this;
	}
	
	public CreateLeadPage enterLnm (String Lnm) {
		type(eleLNm,Lnm);
		return this;
	}
	
	
	public CreateLeadPage enterSal (String sal) {
		type(eleSal,sal);
		return this;
	}
	
	public CreateLeadPage enterPhnum (String pnm) {
		type(elePh,pnm);
		return this;
	}
	
		
		public ViewLeadPage ClickSubmit() {
			click(eleClButton);
			return new ViewLeadPage();
	
	}
	
	
}