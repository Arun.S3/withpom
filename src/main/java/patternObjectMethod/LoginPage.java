package patternObjectMethod;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.TC00PackageMethod;

public class LoginPage extends TC00PackageMethod {
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	// page Factory @FindBy -->similar to your locate Elements
	
	@FindBy(how=How.ID, using="username") WebElement eleUserName;
	@FindBy(how=How.ID, using="password")WebElement elePassword;
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")WebElement eleLogin;
	
	//Enter Username
	
	public LoginPage enterusername(String uName) {
		type(eleUserName,uName);
		return this;
	}
	
		
		public LoginPage enterpassword(String password) {
			type(elePassword,password);
			return this;
		}
	

		public Homepage ClickSubmit() {
			click(eleLogin);
			return new Homepage();
	
	}
	
	
}
