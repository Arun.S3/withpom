package patternObjectMethod;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.TC00PackageMethod;

public class MyHomePage extends TC00PackageMethod {

	public MyHomePage() {

		PageFactory.initElements(driver, this);
}

// page Factory @FindBy -->similar to your locate Elements

@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCLeads;

public CreateLeadPage ClickSubmit() {
	click(eleCLeads);
	return new CreateLeadPage();
}}