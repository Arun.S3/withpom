package wdMethods;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ExcelRead extends TC00PackageMethod{

	//@Test

	public static Object[][] createLead(String DataSheetName) throws IOException

	{
		// go to the workbook
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+DataSheetName+".xlsx");

		// go to the sheet
		XSSFSheet sheet = wb.getSheetAt(0);

		// go to the row
		int rowcount = sheet.getLastRowNum();
		System.out.println(rowcount);
		int cellcount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellcount);
		
		Object[] [] data = new Object[rowcount] [cellcount];

		// go to the cell

		for (int i = 1; i <= rowcount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < cellcount; j++) {
				XSSFCell cell = row.getCell(j);
				String cellValue = cell.getStringCellValue();
				System.out.println(cellValue);
				data[i-1][j]=cellValue;
				
			}
		}
		return data;

	}

}

