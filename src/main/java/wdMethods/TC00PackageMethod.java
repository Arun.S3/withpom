package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;


public class TC00PackageMethod extends SeMethods {
	public String datasheetname;
	


	@BeforeMethod
	
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		
	}

	//@AfterMethod
	public void closebrowser() {

		closeBrowser();
	}

	
	@DataProvider(name = "fetchData")
	public Object [][] fetchData() throws IOException {
		Object[][] data = ExcelRead.createLead(datasheetname);
		return data;
		/*String[][] data = new String[2][4];
		data[0][0] = "capgemini";
		data[0][1] = "manoj";
		data[0][2] = "j";
		data[0][3] = "Mr";
		
		data[1][0] = "scb";
		data[1][1] = "arun";
		data[1][2] = "s";
		data[1][3] = "Mr";
*/
		//return data;
			
			
}

}
